package com.ex.ood;

public final class Ch5FixedPointV1 {
	private int x;
	private int y;

	public Ch5FixedPointV1(int x, int y) {
		this.x=x;
		this.y=y;
	}

	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
}
