package com.ex.ood;

import java.util.Observable;

class Ch5ObsPatternModel extends Observable {
	public void changeSomething() {
		setChanged();
		notifyObservers();
	}
}
