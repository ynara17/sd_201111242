package com.ex.ood;

public class Ch7GarageDoorDownCommand implements Ch7Command {
	Ch7GarageDoor gDoor;

	public Ch7GarageDoorDownCommand(Ch7GarageDoor garageDoor) {
		this.gDoor = garageDoor;
	}

	public void execute() {
		gDoor.down();
	}
}
