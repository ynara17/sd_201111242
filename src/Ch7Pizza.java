package com.ex.ood;

import java.util.ArrayList;

public abstract class Ch7Pizza {
	String name;
	String dough;
	String sauce;

	ArrayList toppings = new ArrayList();

	void prepare() {
		System.out.println("  Pizza preparing..");
	}
	void bake() {
		System.out.println("  Baking..");
	}
	void cut() {
		System.out.println("  Cutting..");
	}
	void box() {
		System.out.println("  Boxing..");
	}
	public String getName() {
		return this.name;
	}
}
