package com.ex.ood;

public class Ch7SingPatternLogger {
	private static Ch7SingPatternLogger uniqueInstance;

	private Ch7SingPatternLogger() {}

	public static Ch7SingPatternLogger getInstance() {
		if(uniqueInstance == null)
			uniqueInstance = new Ch7SingPatternLogger();
		return uniqueInstance;
	}

	public void readEntireLog() {
		System.out.println("  Singleton log goes here");
	}
}

