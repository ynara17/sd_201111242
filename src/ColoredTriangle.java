package com.ex.ood;

import com.ex.ood.Triangle;
import java.awt.Color;
import java.awt.Point;

public class ColoredTriangle extends Triangle {
	private Color color;
	
	public ColoredTriangle(Color c, Point p1, Point p2, Point p3) {
		super(p1, p2, p3);
		if(c == null) c = Color.red;	
		color = c;
	}

	public boolean equals(Object obj) {
		if(!(obj instanceof ColoredTriangle)) return false;

		ColoredTriangle otherColoredTriangle = (ColoredTriangle) obj;
                return super.equals(otherColoredTriangle) && 
			this.color.equals(otherColoredTriangle.color);
	}
}
