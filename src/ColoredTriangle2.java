package com.ex.ood;

import com.ex.ood.Triangle;
import java.awt.Color;
import java.awt.Point;

public class ColoredTriangle2 extends Triangle {
        private Color color;

        public ColoredTriangle2(Color c, Point p1, Point p2, Point p3) {
                super(p1, p2, p3);
                if(c == null) c = Color.red;
                color = c;
        }
	
	public boolean equals(Object obj) {
		if(obj instanceof ColoredTriangle2) {
		ColoredTriangle2 otherColoredTriangle2 = (ColoredTriangle2) obj;
		return super.equals(otherColoredTriangle2) &&
		this.color.equals(otherColoredTriangle2.color);
		}
		else if(obj instanceof Triangle) {
		return super.equals(obj);
		}
		else return false;
	}
}
