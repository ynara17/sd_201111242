package com.ex.ood;

import com.ex.ood.Triangle2;
import java.awt.Color;
import java.awt.Point;

public class ColoredTriangle3 extends Triangle2 {
        private Color color;

        public ColoredTriangle3(Color c, Point p1, Point p2, Point p3) {
                super(p1, p2, p3);
                if(c == null) c = Color.red;
                color = c;
        }

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(obj.getClass() != this.getClass()) return false;
		if(!super.equals(obj)) return false;

		ColoredTriangle3 otherColoredTriangle3 = (ColoredTriangle3) obj;
		return this.color.equals(otherColoredTriangle3.color);
	}
}
