package com.ex.ood;

public interface Comparator {
	public int compare(Object o1, Object o2);
	public boolean equals(Object o);
}
