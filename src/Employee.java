package com.ex.ood;

import com.ex.ood.Person;

public class Employee {
	private Person me;
	private float salary;

	public Employee(Person person, float f) {
		this.me = person;
		this.salary = f;
	}
	public String getName() {
                return me.getName();
        }
        public String getAddress() {
                return me.getAddress();
        }
	public float getSalary() {
		return this.salary;
	}
	public void setPerson(Person person) {
		this.me = person;
	}
	public void setSalary(float f) {
		this.salary = f;
	}
}
