package com.ex.ood;

import com.ex.ood.*;
import java.awt.*;
import java.awt.event.*;

public class Graphic extends Frame {
	Oval ov1 = new Oval(20, 30, 40, 40);
	Oval ov2 = new Oval(40, 50, 30, 40);
	FilledOval fildov = new FilledOval(60, 60, 50, 40);

	public void paint(Graphics g) {
		ov1.draw(g);
		ov2.draw(g);
		fildov.draw(g);
		}

public Graphic() {
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
			dispose();
			System.exit(0);
			}
		});
	}
}
	
