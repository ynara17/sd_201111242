package com.ex.ood;

import com.ex.ood.*;
import java.util.*;
import java.util.Arrays;
import java.util.ArrayList;

public class LinkedArray {
	List<String> list;

	public LinkedArray(List list) {
		this.list = list;
	}

	public void addList(String str) {
		list.add(str);
	}

	public void sizeList() {
		System.out.println(list.size());
	}

	public void clearList() {
		list.clear();
	}

	public void iter() {
		Iterator<String> Itr = list.iterator();
	
		while(Itr.hasNext()) {
			System.out.println(Itr.next());
		}
	}
}
