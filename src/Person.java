package com.ex.ood;

public class Person {
	private String name;
	private String address;

	public Person(String e, String j) {
		this.name = e;
		this.address = j;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public void setName(String e) {
		this.name = e;
	}
	public void setAddress(String j) {
		this.address = j;
	}
}
