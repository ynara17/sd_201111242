package com.ex.ood;

public class Refactoring3 {
	public double getRoomCharge() {
		return 100;
	}

	public double getTotalBill() {
		final double roomCharge = getRoomCharge();
			double mealCharge = 20;
			double movieCharge = 5;
			return roomCharge + mealCharge + movieCharge;
	}
}
