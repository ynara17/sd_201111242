package com.ex.ood;

public class Refactoring4 {
	public double getRoomCharge() {
		return 100;
	}

	public double getMealCharge() {
		return 20;
	}

	public double getMovieCharge() {
		return 5;
	}
	
	public double getTotalBill() {
		return getRoomCharge() + getMealCharge() + getMovieCharge();
	}
}
