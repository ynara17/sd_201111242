package com.ex.ood;

import javax.swing.JFrame;
import com.ex.ood.*;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Color;
import java.util.*;

public class SD_2_201111242_Tester {
	public static void main(String[] args){
		ch8main();
	//	ch7main();
	//	ch5main();
	//	ch4main();
	//	ch3main();
	//	ch2main();
	}

	public static void ch8main() {
		//drawing test
		System.out.println("*** This is a ch8 Test ***");
	//	Ch8SimpleDrawMain drawFrame = new Ch8SimpleDrawMain();
	//	drawFrame.setVisible(true);
		
		//button listener-push counter
	//	JFrame frame = new JFrame("Push Counter");
	//	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//	Ch8PushCounterPanel panel = new Ch8PushCounterPanel();
	//	frame.getContentPane().add(panel);
	//	frame.pack();
	//	frame.setVisible(true);
	}

	public static void ch7main() {
		System.out.println("*** This is a ch7 Test ***");
		System.out.println("# Using Before Singleton Pattern");
		Ch7SingPatternLoggerBefore bf1 = new Ch7SingPatternLoggerBefore();
		bf1.readEntireLog();
		Ch7SingPatternLoggerBefore bf2 = new Ch7SingPatternLoggerBefore();
		bf2.readEntireLog();
		Ch7SingPatternLoggerBefore bf3 = new Ch7SingPatternLoggerBefore();       
       		bf3.readEntireLog();

		System.out.println("# Using Singleton Pattern");
		//Ch7SingPatternLogger bf4 = new Ch7SingPatternLogger();
		//bf4.readEntireLog();
		Ch7SingPatternLogger bf5 = Ch7SingPatternLogger.getInstance();
		bf5.readEntireLog();

		System.out.println("# Command Pattern");
		Ch7SimpleRemoteControl remote = new Ch7SimpleRemoteControl();
		Ch7Light lit = new Ch7Light();
		Ch7GarageDoor door  = new Ch7GarageDoor();

		Ch7LightOnCommand lighton = new Ch7LightOnCommand(lit);
		remote.setCommand(lighton);
		remote.buttonWasPressed();
		
		Ch7LightOffCommand lightoff = new Ch7LightOffCommand(lit);
		remote.setCommand(lightoff);
		remote.buttonWasPressed();

		Ch7GarageDoorOpenCommand dooropen = new Ch7GarageDoorOpenCommand(door);
		remote.setCommand(dooropen);
		remote.buttonWasPressed();

		Ch7GarageDoorDownCommand doordown = new Ch7GarageDoorDownCommand(door);
		remote.setCommand(doordown);
		remote.buttonWasPressed();
	
		System.out.println("# Factory Pattern");
		Ch7SimplePizzaFactory factory = new Ch7SimplePizzaFactory();
		Ch7PizzaStore ps = new Ch7PizzaStore(factory);
		Ch7CheesePizza cp = new Ch7CheesePizza();
		ps.orderPizza("cheese");	
	}

	public static void ch5main() {
		System.out.println("*** This is a ch5 Test ***");
		System.out.println("# V1 FixedPoint");//immutable
		Ch5FixedPointV1 fp1 = new Ch5FixedPointV1(3, 4);
 		System.out.println(fp1.getX());
		// fp1.x = 5;
		
		System.out.println("# V2 FixedPoint-Inheritance");//immutable
		Ch5FixedPointV2 fp2 = new Ch5FixedPointV2(3, 4);
		System.out.println(fp2.getX());
		fp2.x = 5;
		System.out.println(fp2.getX());
		
		System.out.println("# V3 FixedPoint-Association");//mutable
		Ch5FixedPointV3 fp3 = new Ch5FixedPointV3(3, 4);
                System.out.println(fp3.getX());
                // fp3.x = 5;
                System.out.println(fp3.getX());

		System.out.println("# Observer Pattern");
		Ch5ObsPatternView view = new Ch5ObsPatternView();
		Ch5ObsPatternModel model = new Ch5ObsPatternModel();
		model.addObserver(view);
		model.changeSomething();

		System.out.println("# Interface Pattern");
		Ch5ArrayLinkedList arr = new Ch5ArrayLinkedList();
		Ch5ArrayLinkedList link = new Ch5ArrayLinkedList();
		arr.printArrayList();
		link.printLinkedList();	
	}

	public static void ch4main() {
		System.out.println("*** This is a ch4 Test ***");
		System.out.println("4-2 Equals");
		System.out.println("Version_01");
		Triangle t = new Triangle(new Point(0,0),
				new Point(1,1), new Point(2,2));
		ColoredTriangle ct01 = new ColoredTriangle(Color.red, 
			new Point(0,0), new Point(1,1), new Point(2,2));
		ColoredTriangle ct02 = new ColoredTriangle(Color.blue,
			new Point(0,0), new Point(1,1), new Point(2,2));
//		System.out.println("  # Reflexive");
//		System.out.println("	"+t.equals(t));
//		System.out.println("	"+ct01.equals(ct01));
//		System.out.println("	"+ct02.equals(ct02));
		System.out.println("  # Symmertic");
		System.out.println("	"+t.equals(ct01));
                System.out.println("	"+ct01.equals(t));
//		System.out.println("  # Transitive");
//              System.out.println("	"+ct01.equals(t));
//              System.out.println("	"+t.equals(ct02));
//		System.out.println("	"+ct01.equals(ct02));
//		System.out.println("  # Consistent");
//              System.out.println("	"+t.equals(ct01));
//              System.out.println("  # NonNull");
//		System.out.println("	"+t.equals(null));
//		System.out.println("	"+ct01.equals(null));
//		System.out.println("	"+ct02.equals(null));

		System.out.println("Version_02");
                Triangle tt = new Triangle(new Point(0,0),
                                new Point(1,1), new Point(2,2));
                ColoredTriangle2 ct03 = new ColoredTriangle2(Color.red,
                        new Point(0,0), new Point(1,1), new Point(2,2));
                ColoredTriangle2 ct04 = new ColoredTriangle2(Color.blue,
                        new Point(0,0), new Point(1,1), new Point(2,2));
//              System.out.println("  # Reflexive");
//              System.out.println("	"+tt.equals(tt));
//              System.out.println("	"+ct03.equals(ct03));
//              System.out.println("	"+ct04.equals(ct04));
//		System.out.println("  # Symmertic");
//              System.out.println("	"+tt.equals(ct03));
//              System.out.println("	"+ct03.equals(tt));
                System.out.println("  # Transitive");
                System.out.println("	"+ct03.equals(tt));
                System.out.println("	"+tt.equals(ct04));
                System.out.println("	"+ct03.equals(ct04));
//              System.out.println("  # Consistent");
//              System.out.println("	"+tt.equals(ct03));
//              System.out.println("  # NonNull");
//              System.out.println("	"+tt.equals(null));
//              System.out.println("	"+ct03.equals(null));
//		System.out.println("	"+ct04.equals(null));

		System.out.println("Version_03");
                Triangle2 ttt = new Triangle2(new Point(0,0),
                                new Point(1,1), new Point(2,2));
                ColoredTriangle3 ct05 = new ColoredTriangle3(Color.red,
                        new Point(0,0), new Point(1,1), new Point(2,2));
                ColoredTriangle3 ct06 = new ColoredTriangle3(Color.blue,
                        new Point(0,0), new Point(1,1), new Point(2,2));
//              System.out.println("  # Reflexive");
//              System.out.println("	"+ttt.equals(ttt));
//              System.out.println("	"+ct05.equals(ct05));
//		System.out.println("	"+ct06.equals(ct06));
                System.out.println("  # Symmertic");
                System.out.println("	"+ttt.equals(ct05));
                System.out.println("	"+ct05.equals(ttt));
                System.out.println("  # Transitive");
                System.out.println("	"+ct05.equals(ttt));
                System.out.println("	"+ttt.equals(ct06));
                System.out.println("	"+ct05.equals(ct06));
//              System.out.println("  # Consistent");
//              System.out.println("	"+ttt.equals(ct05));
//              System.out.println("  # NonNull");
//              System.out.println("	"+ttt.equals(null));
//              System.out.println("	"+ct05.equals(null));
//		System.out.println("	"+ct06.equals(null));
	
		System.out.println("4-3 Refactoring");
		System.out.println("Rename method refactoring");
		Refactoring rf = new Refactoring();
		Refactoring2 rf2 = new Refactoring2();
		System.out.println("  # getRoomBill : " + rf.getRoomCharge());
		System.out.println("  # getTotalBill : " + rf2.getTotalBill());

		System.out.println("Introduce explainig variable refactoring");
		Refactoring3 rf3 = new Refactoring3();
		System.out.println("  Room Charge : " + rf3.getRoomCharge());
		System.out.println("  # Total Bill : " + rf3.getTotalBill());
		
		System.out.println("Replace temp with query refactoring");
		Refactoring4 rf4 = new Refactoring4();
		System.out.println("  Room Charge : " + rf4.getRoomCharge());
		System.out.println("  Meal Charge : " + rf4.getMealCharge());
		System.out.println("  Movie Charge : " + rf4.getMovieCharge());
		System.out.println("  # Total Bill = " + rf4.getTotalBill());
	}

        public static void ch3main() {
                System.out.println("*** This is a ch3 Test ***");
                System.out.println("3-1 Rectangle, MutableSquare");
                mutableRectangle murec = new mutableRectangle(10, 10, 20, 20);
                mutableSquare musqu = new mutableSquare(30, 20, 20);
		System.out.println("  # mutableRectangle");
                System.out.println("	Width : " + murec.getWidth());
                System.out.println("	Height : " + musqu.getHeight());
		System.out.println("  # mutableSquare");
		System.out.println("	Width : " + musqu.getWidth());
		System.out.println("	Height : " + musqu.getHeight());

                System.out.println("3-2 Person, Student");
                Person me = new Person("A", "a");
		Student std = new Student(me, (float) 10.5);
		Employee ep = new Employee(me, (float) 350.5);
		System.out.println("  # Student's info");
		System.out.println("	Name : " + std.getName());
		System.out.println("	GPA : " + std.getGPA());
		System.out.println("  # Employee's info");
		System.out.println("	Name : " + ep.getName());
		System.out.println("	Salary : " + ep.getSalary()); 

                System.out.println("3-4 Sorter");
                String[] B = {"John", "Adams", "Skrien", "Smith", "Jones"};
                Comparator stringComp = new StringComparator();
                Sorter.sort(B, stringComp);
		for(int i = 0; i < B.length; i++)
		System.out.println(B[i]);

                Integer[] C = {new Integer(3), new Integer(1)};
                Comparator integerComp = new IntegerComparator();
                Sorter.sort(C, integerComp);
		for(int i = 0; i < C.length; i++)
		System.out.println(C[i]);
	}

	public static void ch2main() {
		System.out.println("*** This is a ch2 test ***");
		System.out.println("2-2 EnhancedRectangle & Ovals");
		EnhancedRectangle rectang = new EnhancedRectangle(1, 1, 30, 30);
		rectang.setLocation(5, 5);
		rectang.setCenter(30, 30);

		Graphic f = new Graphic();
		f.setSize(200, 200);
		f.setVisible(true);

		System.out.println("2-3 LinkedList vs ArrayList");
		LinkedArray la = new LinkedArray(new ArrayList<String>());
		System.out.println("# Add List");
		la.addList("Aa");
		la.addList("Bb");
		la.addList("Cc");
		la.iter();

		System.out.println("# Size of List");
		la.sizeList();
		System.out.println("# Clear");
		la.clearList();

		System.out.println("2-4 Automobile Capacity");
		int TotalCap = 0;
		Automobile[] fleet = new Automobile[3];
		fleet[0] = new Sedan(5);
		fleet[1] = new Minivan(10);
		fleet[2] = new SportsCar(3);

		for(int i=0; i<fleet.length; i++) {
			if(fleet[i] instanceof Sedan)
			TotalCap += ((Sedan) fleet[i]).getCapacity();
			else if(fleet[i] instanceof Minivan)
			TotalCap += ((Minivan) fleet[i]).getCapacity();
			else if(fleet[i] instanceof SportsCar)
			TotalCap += ((SportsCar) fleet[i]).getCapacity();
			else TotalCap += fleet[i].getCapacity();
		}

		System.out.println("# VS1.TotalCap = " + TotalCap);
		TotalCap = 0;

		for(int i=0; i<fleet.length; i++)
		TotalCap += fleet[i].getCapacity();

		System.out.println("# VS2.TotalCap = " + TotalCap);
	}
}
