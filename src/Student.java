package com.ex.ood;

import com.ex.ood.Person;

public class Student {
	private Person me;
	private float GPA;
	
	public Student(Person person, float f) {
		this.me = person;
		this.GPA = f;
	}
	public String getName() {
		return me.getName();
	}
	public String getAddress() {
		return me.getAddress();
	}
	public float getGPA() {
		return this.GPA;
	}
	public void setPerson(Person person) {
		this.me = person;
	}
	public void setGPA(float f) {
		this.GPA = f;
	}
}
