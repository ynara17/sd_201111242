package com.ex.ood;

public class ch3Rectangle {
	private int width, height, x, y;
	public ch3Rectangle(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		width = w;
		height = h;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height; 
	}
	public int getArea() {
		return width * height; 
	}
	public int getPerimeter() { 
		return 2 * (width + height); 
	}
	public void setTopLeft(int newx, int newy) { 
		x = newx; 
		y = newy; 
	}

 	public void setWidth(int w) {
		this.width = w;
	}

	public void setHeight(int h) {
		this.height = h;
	}
//	public erase(Graphics g) {}
//	public draw(Graphics g) {}
//	public void setSize(int w, int h) { 
//		width = w; 
//		height = h; 
//	}
}
