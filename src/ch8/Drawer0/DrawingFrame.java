package com.ex.ood.ch8.Drawer0;

import javax.swing.*;
import java.awt.*;

/*
	@author 201111242
	@version 1.0 20131118
*/

public class DrawingFrame extends JFrame {
	public DrawingFrame() {
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JComponent drawingCanvas = createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);

		JToolBar toolbar = createToolbar();
		add(toolbar, BorderLayout.NORTH);
	}
/*
	creates the canvas
	the part of the window below the tool bar
	@return the new JComponent that is the canvas
*/
	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}
/*
	creates the tool bar with the three buttons
	@return the new tool bar
*/

	private JToolBar createToolbar() {
		JToolBar toolbar = new JToolBar();
		JButton ellipseButton = new JButton("Ellipse");
		toolbar.add(ellipseButton);
		JButton squareButton = new JButton("Square");
                toolbar.add(squareButton);
		JButton rectButton = new JButton("Rect");
                toolbar.add(rectButton);
		return toolbar;
	}		
}
