package com.ex.ood.ch8.Drawer1;

import javax.swing.*;
import java.awt.*;

/*
	@author 201111242
	@version 1.0 20131118
*/

public class DrawingFrame extends JFrame {
	public DrawingFrame() {
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JComponent drawingCanvas = createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);

		JToolBar toolbar = createToolbar(drawingCanvas);
		add(toolbar, BorderLayout.NORTH);
	}
/*
	creates the canvas
	the part of the window below the tool bar
	@return the new JComponent that is the canvas
*/
	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}
/*
	creates the tool bar with the three buttons
	@return the new tool bar
*/

	private JToolBar createToolbar(JComponent canvas) {
		JToolBar toolbar = new JToolBar();
		//add the buttons to the toolbar
		JButton ellipseButton = new JButton("Ellipse");
		toolbar.add(ellipseButton);
		JButton squareButton = new JButton("Square");
                toolbar.add(squareButton);
		JButton rectButton = new JButton("Rect");
                toolbar.add(rectButton);

		//add the CanvasEditor listener to the canvas and to the buttons
		//with the ellipseButton initially selected
		CanvasEditor canvasEditor = new CanvasEditor(ellipseButton);
		ellipseButton.addActionListener(canvasEditor);
		squareButton.addActionListener(canvasEditor);
		rectButton.addActionListener(canvasEditor);
		canvas.addMouseListener(canvasEditor);

		return toolbar;
	}	
}
