package com.ex.ood.ch8.Drawer2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import com.ex.ood.ch8.Drawer2.figure.*;

public class CanvasEditor implements MouseListener {
	private Figure currentFigure;

	public CanvasEditor(Figure figure) {
		this.currentFigure = figure;
	}
	public void setCurrentFigure(Figure newFigure) {
		currentFigure = newFigure;
	}
	public void mouseClicked(MouseEvent e) {		
		JPanel canvas = (JPanel) e.getSource();
		currentFigure.setCenter(e.getX(), e.getY());
		currentFigure.draw(canvas.getGraphics());	
	}
	public void actionPerforemd(ActionEvent e) {
		JButton currentButton = (JButton) e.getSource();
			if(currentButton.getText().equals("Ellipse"))
			currentFigure = new Ellipse(0,0,60,40);
			else if(currentButton.getText().equals("Rect"))
			currentFigure = new Rect(0,0,60,40);
			else//if(currentButton.getText().equals("Square"))
			currentFigure = new Square(0,0,50);
	}
	//ignore mouse press, release, enter, and exit events
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
}
