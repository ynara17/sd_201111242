package com.ex.ood.ch8.Drawer2.figure;

import java.awt.*;

public class Ellipse extends Figure {
	public Ellipse(int x, int y, int w, int h) {
		super(x, y, w, h);
	}
	public void draw(Graphics g) {
		int width = getWidth();
		int height = getHeight();
		g.drawOval(getCenterX() - width/2, getCenterY() - height/2,
		width, height);
	}
}

