package com.ex.ood.ch8.Drawer3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.ex.ood.ch8.Drawer3.figure.*;
import com.ex.ood.ch8.Drawer3.*;

/*
	@author 201111242
	@version 2.0 20131125
*/

public class DrawingFrame extends JFrame {
	public DrawingFrame() {
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	//	JComponent drawingCanvas = createDrawingCanvas();
		JPanel drawingCanvas = new DrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);		

		JToolBar toolbar = createToolbar(drawingCanvas);
		add(toolbar, BorderLayout.NORTH);
	}
/*
	creates the canvas
	the part of the window below the tool bar
	@return the new JComponent that is the canvas
*/
	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}
/*
	creates the tool bar with the three buttons
	@return the new tool bar
*/

	private JToolBar createToolbar(JComponent canvas) {
		JToolBar toolbar = new JToolBar();
		//add the buttons to the toolbar
		JButton ellipseButton = new JButton("Ellipse");
		toolbar.add(ellipseButton);
		JButton squareButton = new JButton("Square");
                toolbar.add(squareButton);
		JButton rectButton = new JButton("Rect");
                toolbar.add(rectButton);
		final CanvasEditor canvasEditor =
			new CanvasEditor(new Ellipse(0,0,60,40));
		
		ellipseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				canvasEditor.setCurrentFigure(new Ellipse(0,0,60,40));
			}
		});
		
		rectButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                                canvasEditor.setCurrentFigure(new Rect(0,0,60,40));
                        }
                });
		
		squareButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                                canvasEditor.setCurrentFigure(new Square(0,0,50));
                        }
                });

		canvas.addMouseListener(canvasEditor);

		return toolbar;
	}
	
}
