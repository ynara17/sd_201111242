package com.ex.ood.ch8.Drawer3.figure;

import java.awt.*;

public abstract class Figure implements Cloneable {
	private int centerX, centerY;
	private int width;
	private int height;

	public Figure(int centerX, int centerY, int w, int h) {
		this.centerX = centerX;
		this.centerY = centerY;
		this.width = w;
		this.height = h;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setCenter(int centerX, int centerY) {
		this.centerX = centerX;
		this.centerY = centerY;
	}
	public int getCenterX() {
		return centerX;
	}
	public int getCenterY() {
		return centerY;
	}
	public abstract void draw(Graphics g);
	
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			assert false; //this code block should never execute
			return null;
		}
	}
}

