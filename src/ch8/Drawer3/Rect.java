package com.ex.ood.ch8.Drawer3.figure;

import java.awt.*;
import com.ex.ood.ch8.*;

public class Rect extends Figure {
        public Rect(int x, int y, int w, int h) {
                super(x, y, w, h);
        }
        public void draw(Graphics g) {
                int width = getWidth();
                int height = getHeight();
                g.drawRect(getCenterX() - width/2, getCenterY()-height/2,
                width, height);
        }
}

