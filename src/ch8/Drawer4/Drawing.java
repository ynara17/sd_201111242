package com.ex.ood.ch8.Drawer4;

import java.awt.*;
import com.ex.ood.ch8.Drawer4.figure.Figure;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Drawing implements Iterable<Figure> {
	private List<Figure> figures; //collection of Figures
	
	public Drawing() {
		figures = new ArrayList<Figure>();
	}
	public void addFigure(Figure newFigure) {
		figures.add(newFigure);
	}
	public Iterator<Figure> iterator() {
		return figures.iterator();
	}
}
