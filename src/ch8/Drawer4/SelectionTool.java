package com.ex.ood.ch8.Drawer4;

import com.ex.ood.ch8.Drawer4.tool;
import com.ex.ood.ch8.Drawer4.DrawingCanvas;
import com.ex.ood.ch8.Drawer4.figure.Figure;
import java.awt.*;
import java.awt.event.MouseEvent;

public class SelectionTool extends Tool {
	private Point origin;
	private boolean pressedInFigure;

	public SelectionTool() {
		origin = new Point(0,0);
		pressedInFigure = false;
	}
	public void mouseClicked(MouseEvent e) {
		DrawingCanvas canvas = (DrawingCanvas) e.getSource();
		Figure figure = canvas.getFigureContaining(e.getX(), e.getY());
		pressedInFigure = (figure != null);
		if(! pressedInFigure)
			canvas.unselectAll();
		if(pressedInFigure && ! figure.isSelected())
			canvas.unselectAll();
		if(pressedInFigure)
			figure.setSelected(true);
		origin.x = e.getX();
		origin.y = e.getY();
	}
	public void mousePressed(MouseEvent e) {
		mouseClicked(e);
	}
	public void mouseReleased(MouseEvent e) {
		DrawingCanvas canvas = (DrawingCanvas) e.getSource();
		canvas.shrinkSelectionRect();
	}
	public void mouseDragged(MouseEvent e) {
		if(pressedInFigure)
			updateDraggingFigures(e);
		else 
			updateSelectionRect(e);
	}

	private void updateDraggingFigures(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		DrawingCanvas canvas = (DrawingCanvas) e.getSource();
		canvas.moveSelectedFigures(x-origin.x, y-origin.y);
		origin.x = x;
		origin.y = y;
	}
	private void updateSelectionRect(MouseEvent e) {
		DrawingCanvas canvas = (DrawingCanvas) e.getSource();
		canvas.setSelectionRect(origin, e.getPoint());
		canvas.selectFiguresIntersectingRect();
	}
}

