package com.ex.ood.ch8.Drawer4;

/*	
	creates and displays a DrawingFrame object
	@param args
*/
public class main {
	public static void main(String[] args) {
        	DrawingFrame drawFrame = new DrawingFrame();
        	drawFrame.pack();
        	drawFrame.setVisible(true);
	}
}
