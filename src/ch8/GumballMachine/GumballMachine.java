package com.ex.ood;

public class GumballMachine {
	State soldOutState;
	State noQuarterState;
	State hasQuarterState;
	State soldState;

	State state = soldOutState;
	int count = 0;
	
	public GumballMachine(int numberGumballs) {
		soldOutState = new SoldOutState(this);
		noQuarterState = new NoQuarterState(this);
		hasQuarterState = new HasQuarterState(this);
		soldState = new SoldState(this);
		this.count = numberGumballs;
		if(numberGumballs > 0) {
			state = noQuarterState;
		}
	}
	public void printGumball(){
	//	System.out.println("주식회사 왕뽑기");
	//	System.out.println("우분투로 돌아가는 뽑기기계");
	//	System.out.println("남은 개수 : "+ count + "개");
	//	System.out.println("동전 투입 대기중");
	}
	public void insertQuarter() {
		state.insertQuarter();
	}
	public void ejectQuarter() {
		state.ejectQuarter();
	}
	public void turnCrank() {
		state.turnCrank();
		state.dispense();
	}
	void setState(State state) {
		this.state = state;
	}

	State getSoldState(){
		this.state = soldState;
		return this.state;
	}
	State getHasQuarterState(){
		this.state = hasQuarterState;
		return this.state;
	}
	State getNoQuarterState(){
		this.state = noQuarterState;
		return this.state;
	}
	State getSoldOutState(){
		this.state = soldOutState;
		return this.state;
	}
	int getCount(){
		return count;
	}
	void releaseBall() {
		System.out.println("A gumball comes rolling out the slot..");
		if(count != 0) {
			count = count -1;
		}
	}
}

