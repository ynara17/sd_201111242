package com.ex.ood;

public class SoldOutState implements State{
	GumballMachine gumballMachine;

	public SoldOutState(GumballMachine gumballMachine){
		this.gumballMachine = gumballMachine;
	}
	public void insertQuarter(){
		System.out.println("Do not insert coin..");
	}
	public void ejectQuarter(){
		System.out.println("Sold out!");
	}
	public void turnCrank(){
		System.out.println("Sold out!");
	}
	public void dispense(){
		System.out.println("Sold out!");
	}
}
