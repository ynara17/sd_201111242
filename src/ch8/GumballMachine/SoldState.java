package com.ex.ood;

public class SoldState implements State {
	GumballMachine gumballMachine;

	public SoldState(GumballMachine gumballMachine){
		this.gumballMachine = gumballMachine;
	}
	public void insertQuarter() {
		System.out.println("잠시만 기다려 주세요. 알맹이가 나갑니다");
	}
	public void ejectQuarter() {
		System.out.println("이미 알맹이를 뽑으셨습니다.");
	}
	public void turnCrank() {
		gumballMachine.releaseBall();
		if(gumballMachine.getCount() > 0) {
			gumballMachine.setState(gumballMachine.getNoQuarterState());
		} else {
			System.out.println("oops, out of gumballs!");
			gumballMachine.setState(gumballMachine.getSoldOutState());
		}
	}
	public void dispense() {}
}
