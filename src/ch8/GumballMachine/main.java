package com.ex.ood;

public class main{
	public static void main(String[] args){
		GumballMachine gumballMachine = new GumballMachine(5);
		gumballMachine.printGumball();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.printGumball();
		gumballMachine.insertQuarter();
		gumballMachine.ejectQuarter();
		gumballMachine.turnCrank();
		gumballMachine.printGumball();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.ejectQuarter();
		gumballMachine.printGumball();
		gumballMachine.insertQuarter();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.printGumball();
	}
}
