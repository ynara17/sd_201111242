package com.ex.ood;

import com.ex.ood.ch3Rectangle;

public class mutableRectangle extends ch3Rectangle {
	public mutableRectangle(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public void setSize(int w, int h) {
		super.setWidth(w);
		super.setHeight(h);
	}

	public int getWidth() {
		return super.getWidth();
	}

	public int getHeight() {
		return super.getHeight();
	}
}
	
