package com.ex.ood;

import com.ex.ood.ch3Rectangle;

public class mutableSquare extends ch3Rectangle {
	public mutableSquare(int x, int y, int side) {
		super(x, y, side, side);
	}

	public void setSize(int s) {
		super.setWidth(s);
		super.setHeight(s);
	}

	public int getWidth() {
		return super.getWidth();
	}
	
	public int getHeight() {
		return super.getHeight();
	}
}	
